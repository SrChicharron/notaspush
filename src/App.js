import React, { useState } from 'react';
import './App.css';
import './styles/global.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './pages/Home';
import MiPerfil from './pages/MiPerfil';

import Login from './pages/Login';

function App() {
  const [isAuthenticated, setAuthenticated] = useState(false);

  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/*" element={<Home isAuthenticated={isAuthenticated} />} />
          <Route path='/Miperfil' element={<MiPerfil />}  />
          
          <Route path='/login' element={<Login setAuthenticated={setAuthenticated} />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
