
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import {getMessaging} from "firebase/messaging"

const firebaseConfig = {
  apiKey: "AIzaSyBAWwJMiZnV12ZCXDazKw-shghHdTbTpEg",
  authDomain: "notificaciones-push-a4830.firebaseapp.com",
  projectId: "notificaciones-push-a4830",
  storageBucket: "notificaciones-push-a4830.appspot.com",
  messagingSenderId: "626884731519",
  appId: "1:626884731519:web:d3b9e6184f9393748f154a",
  measurementId: "G-HW2C6PQ88S"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const analytics = getAnalytics(app);
export const messaging= getMessaging(app);
