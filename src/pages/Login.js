import React from 'react';
import './Login.css';
import { Link } from 'react-router-dom';
import { app } from '../firebase';
import { getAuth, signInAnonymously } from 'firebase/auth';


//const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

const Login = ({ setAuthenticated }) => {
  const handleAnonymousLogin = async () => {
    try {
      await signInAnonymously(auth);
      setAuthenticated(true);
    } catch (error) {
      console.error('Error al autenticar de forma anónima:', error.message);
    }
  };

  return (
    <div className="login-container">
      <h2>Iniciar Sesión</h2>
      <Link to="/" onClick={handleAnonymousLogin} className="login-button">
        Iniciar sesión anónimamente
      </Link>
    </div>
  );
};

export default Login;
