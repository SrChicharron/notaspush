/* eslint-disable no-restricted-globals */
/* eslint-disable no-undef */
const db = new PouchDB('mensajes');

function guardarMensaje(message) {
  message._id = new Date().toISOString();

  return db.put(message).then(() => {
    self.registration.sync.register('nuevo-post');
    const newResp = { ok: true, offline: true };
    return new Response(JSON.stringify(newResp));
  });

}

//poostear mensajes a la API
function postearMensaje(){
    const posteos = [];

    return db.allDocs({include_docs: true}).then(docs=>{

        docs.rows.forEach(row=>{

            const doc = row.doc;
            const fetchProm = fetch('api', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(doc)
            }).then(res=>{
                return db.remove(doc)
            })
            posteos.push(fetchProm)
            
        })//   fin del forEach

        return Promise.all(posteos)
    })
}