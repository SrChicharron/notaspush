import React, { useState, useEffect } from 'react';
import { Modal, Input, message } from 'antd';
import { PlusOutlined } from '@ant-design/icons'; 

import NotesList from '../components/NotesList';
import Contador from '../components/Contador';
import BuscadorNotas from '../components/BuscadorNotas';
import Login from './Login';
import { getToken,onMessage } from 'firebase/messaging';
import { messaging } from '../firebase';
import {ToastContainer,toast} from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'



const { TextArea } = Input;

function Home() {
  const [isAuthenticated, setAuthenticated] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [modalAddNote, setModalAddNote] = useState(false);
  const [noteText, setNoteText] = useState('');
  const [notesList, setNotesList] = useState([]);
  const [validations, setValidations] = useState({});
  const [contadorNotasCompletadas, setContadorNotasCompletadas] = useState(0);
  const [success, contextHolder] = message.useMessage();

  const getTokenNotification=async ()=>{
    const token= await getToken(messaging,{
      vapidKey:'BBZeFVb7DeSPeVqGjKnsx8-HUJIVl1y3_fegOOUSO1htYxy9cj9CIgYHZoAGNo8AhsMxNNXUctpyM05uLhcYQR8'
    }).catch((err) => console.log('No se puede obtener el token',err))
    if (token){
      console.log('Token:',token)
    }if(!token){
      console.log('No hay token disponible')
    }
  }
const notificarme=()=>{
  if(!window.Notification){
    console.log('Este navegador no soporta notificaciones');
    return;
  }
  if(Notification.permission==='granted'){
    getTokenNotification();//Obtener y moestrar el token en la consola
  }else if(Notification.permission!=='denied'|| Notification.permission==='default'){
    Notification.requestPermission((permission)=>{
      console.log(permission);
      if(permission==='granted'){
        getTokenNotification();//Obtener y mostrar el token en la consola
      }
    })
  }
}
notificarme();
useEffect(()=>{
  getTokenNotification()
  onMessage(messaging,message=>{
    console.log('onMessage',message)
    toast(message.notification.title)
  })
},[])


  const rootStyles = getComputedStyle(document.documentElement);

  const handleChangeInput = (txt) => {
    setNoteText(txt.target.value);
  };

  const handleShowModal = () => {
    setNoteText('');
    setValidations({});
    setModalAddNote(!modalAddNote);
  };

  const saveNote = () => {
    if (noteText.trim() !== '') {
      const id = new Date().getTime().toString();
      const newNote = { id: id, note: noteText, noteDone: false };

      fetch('http://localhost:3001/api/noteAdd', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(newNote),
      })
        .then((response) => {
          if (response.status === 200) {
            setNotesList([...notesList, newNote]);
            setModalAddNote(false);
            successAlert();
            setValidations({});
          } else {
            console.error('Error al guardar la nota');
          }
        })
        .catch((error) => {
          console.error('Error en la solicitud HTTP:', error);
        });
    } else {
      setValidations({
        ...validations,
        inpNote: 'El texto de la nota no puede estar vacío.',
      });
    }
  };

  const getNotes = () => {
    fetch('http://localhost:3001/api/note', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then((response) => {
        if (response.status === 200) {
          response.json()
            .then((data) => {
              console.log(data);
            })
            .catch((jsonError) => {
              console.error('Error al parsear la respuesta JSON:', jsonError);
            });
        } else {
          console.error('Error al obtener las notas');
        }
      })
      .catch((error) => {
        console.error('Error en la solicitud HTTP:', error);
      });
  };

  const completarNota = (noteId) => {
    const newNotes = notesList.map((note) => {
      if (note.id === noteId) {
        setContadorNotasCompletadas(contadorNotasCompletadas + 1);
        return { ...note, noteDone: true };
      }
      return note;
    });
    completedAlert();
    setNotesList(newNotes);
  };

  const successAlert = () => {
    success.open({
      type: 'success',
      content: 'Nota guardada correctamente',
      duration: 6,
    });
  };

  const completedAlert = () => {
    success.open({
      type: 'success',
      content: 'Nota finalizada',
      duration: 2,
    });
  };

  useEffect(() => {
    let notesStorage = localStorage.getItem('notes');
    if (notesStorage) {
      setNotesList(JSON.parse(notesStorage));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem('notes', JSON.stringify(notesList));
  }, [notesList]);

  const eliminarNota = (noteId) => {
    const noteToDelete = notesList.find((note) => note.id === noteId);
    if (noteToDelete && noteToDelete.noteDone) {
      setContadorNotasCompletadas(contadorNotasCompletadas - 1);
    }
    const newNotes = notesList.filter((note) => note.id !== noteId);
    setNotesList(newNotes);
  };

  // Renderiza el componente Login si no está autenticado
  if (!isAuthenticated) {
    return <Login setAuthenticated={setAuthenticated} />;
  }

  return (
    <div className="App">
    <ToastContainer/>
      <h1>Aplicación para crear notas</h1>
      <Contador contadorNotasCompletadas={contadorNotasCompletadas} />
      <BuscadorNotas onSearchChange={(value) => setSearchTerm(value)} />
      <button onClick={getNotes}>GET notas de MongoDB</button>
      <NotesList
        notes={notesList}
        completarNota={completarNota}
        eliminarNota={eliminarNota}
      />
      <Modal
        title="Agregar nota"
        centered
        open={modalAddNote}
        onOk={saveNote}
        okText="Guardar"
        onCancel={() => setModalAddNote(false)}
        cancelText="Cancelar"
        okButtonProps={{
          style: {
            background: rootStyles.getPropertyValue('--color-primario'),
            borderColor: rootStyles.getPropertyValue('--color-primario'),
            color: rootStyles.getPropertyValue('--color-light'),
          },
        }}
      >
        <TextArea
          value={noteText}
          onChange={handleChangeInput}
          rows={3}
          placeholder="Escribe una nota..."
        />
        {'inpNote' in validations && (
          <p className="error-text">{validations.inpNote}</p>
        )}
      </Modal>
      {/* Botón flotante */}
      <button
        onClick={handleShowModal}
        className="float-button"
        style={{
          backgroundColor: rootStyles.getPropertyValue('--color-primario'),
          color: rootStyles.getPropertyValue('--color-light'),
        }}
      >
        <PlusOutlined />
      </button>
      {contextHolder}
    </div>
  );
}

export default Home;
