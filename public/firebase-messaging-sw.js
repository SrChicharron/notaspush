
/* eslint-disable no-restricted-globals */
/* eslint-disable no-undef */
importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-app-compat.js')
importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-messaging-compat.js')


const firebaseConfig = {
  apiKey: "AIzaSyBAWwJMiZnV12ZCXDazKw-shghHdTbTpEg",
  authDomain: "notificaciones-push-a4830.firebaseapp.com",
  projectId: "notificaciones-push-a4830",
  storageBucket: "notificaciones-push-a4830.appspot.com",
  messagingSenderId: "626884731519",
  appId: "1:626884731519:web:d3b9e6184f9393748f154a",
  measurementId: "G-HW2C6PQ88S"
};

// Initialize Firebase
let app = initializeApp(firebaseConfig);
const messaging= firebase.messaging(app); 

messaging.onBackgroundMessage(payload=>{
    console.log('Recibiendo mensaje en segundo plano')
    const tituloNotificacion=payload.notification.title;
    const options={
        body:payload.notification.body,
        icon:'../asset/cloud.png',
    }
    self.registration.showNotification(tituloNotificacion,options)
})